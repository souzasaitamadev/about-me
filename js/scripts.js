// Variables
let pushState = true;
let isDragging = false;
let isSidebarVisible = false;
let startX = 0;
let currentX = 0;
let translateX = 0;
let canActivateDrag = false;

// Constants
const appContainersClass = "app-container";
const menuItemChildSelector = "menu-item-child";
const textColorCssVar = "var(--text-color)";
const textHoverColorCssVar = "var(--hover-color)";
const changeLanguageClassSelector = "translatable";
const selectedLanguagePlaceholderCssSelector = "selected-lang-placeholder";
const mobileSidebar = document.getElementById("mobile-sidebar");
const rightGlow = document.getElementById("draggable-glow");
const barHamburgerIcon = document.getElementsByClassName("bar");

const THRESHOLD_SWIPE_LEFT = -100;
const ANIMATION_MS = 500;

const themes = {
  DARK: {
    key: "DARK",
    value: `
        :root {
            --hover-color: #8351f883;
            --predominant-color: #0E131F;
            --text-color: #fff;
            --misc-color: #8351f883;
            --misc-color-2: #ff399583;
            --lines-color: #8c68c531;
            --box-shadow: rgba(150, 50, 255, 0.2);
        }
    `,
  },
  LIGHT: {
    key: "LIGHT",
    value: `
        :root {
            --hover-color: #E94F37;
            --predominant-color: #fff;
            --text-color: #040403;
            --misc-color: #E94F37;
            --misc-color-2: #353535;
            --lines-color: #cc5b4a3d;
            --box-shadow: #E94F37;
        }
    `,
  },
  STORAGE_KEY: "SELECTED_THEME",
};

const langs = {
  EN: {
    key: "EN",
    values: [
      {
        id: "menu-item-home",
        value: "Home",
      },
      {
        id: "menu-item-career",
        value: "Career",
      },
    ],
    orderText: [
      "Hi! My name is",
      "Lucas",
      "and I'm",
      "23 years old, ",
      "and I work as a ",
      "software developer!",
      "You can",
      "directly",
      "contact me",
      "through",
      "these",
      "channels:",
      "I",
      "have expertise",
      "and",
      "I've worked",
      "with the following technologies:",
      "Go",
      "on",
      "and give a shot to know me better!",
      "Listen a bit of my",
      "musical taste",
      "while you learn about",
      "the companies",
      "and the",
      "projects",
      "that I've participated!",
      "You can expect from me",
      "a great professional",
      "that knows a",
      "little bit of many things",
      "but with a great will",
      "to share",
      "the little that i know",
      "and fascinated to the unknown!",
      "Lekto is a company that I've contributed with my software development skills for a period of about 1 year, during this time I've learned a lot about business rules and technical difficulties.",
      "Database modeling, deploying scalable applications, microservices architecture, performance, security, all during an exceptional and extremely important period for my career!",
      "BSBIOS is the largest biodiesel company in Latin America, and I participated in it as a consultant and software developer with the aim of providing maintenance and improvements to the main sales and negotiation system.",
      "Systems written in AngularJS, PHP and Lumen, Docker and Websockets consumption with third-party API integration were the focus of my challenges.",
      "Expanzio is a management system for large franchises here in Brazil, I worked as a consultant and led the migration of Angular 12 and .NET 3.1 Core technologies.",
      "Starting with Angular 15 and .NET 7, I had a huge period of learning, mainly with applications like Nginx Manager (reverse proxy), Docker, Digital Ocean and integrations.",
      "Implanta Tecnologia is my second home, I currently work here and face the most varied challenges I have ever encountered, from the most current technologies to old technologies.",
      "Definitely a place where I learned a lot about how the IT business world works, in addition to internal projects, with core technologies in .NET, Angular and MySQL.",
      "A large university in Brasilia, at IESB I developed and offered support for the company's internal and external systems, using PHP and Codeigniter, along with HTML, CSS and Javascript, I was able to develop applications that optimize time and provide productivity to the company's employees.",
      "Challenges such as integration and deployment of an API in Node that integrates with a third-party system (Blackboard) were some of my missions, I learned a lot about SQL Server database, JOBS, modeling, queries were a toast!",
      "At Clariens I participate as a consultant and I offer development in NextJS, Node, and I gained a lot of experience in deploying systems for Vercel.",
      "Undoubtedly a great contributor to my development with Node technologies in the backend, integration with Hubspot and understanding of ERP systems (Lyceum).",
    ],
  },
  BR: {
    key: "BR",
    values: [
      {
        id: "menu-item-home",
        value: "Início",
      },
      {
        id: "menu-item-career",
        value: "Portfólio",
      },
    ],
    orderText: [
      "Olá! Meu nome é",
      "Lucas",
      "tenho",
      "23 anos, ",
      "e trabalho como ",
      "desenvolvedor de software!",
      "Você pode ",
      "entrar em",
      "contato",
      "direto",
      "comigo",
      "através dos seguintes canais:",
      "Eu tenho",
      "experiência",
      "e já",
      "trabalhei",
      "com as seguintes tecnologias:",
      "Dá",
      "play",
      "e tira um tempinho pra você conhecer",
      "um pouco do meu gosto",
      "gosto musical",
      "enquanto olha as",
      "empresas",
      "ou os",
      "projetos",
      "em que eu participei!",
      "Você pode esperar de mim",
      "um grande profissional",
      "que sabe",
      "um pouco de muita coisas,",
      "mas que possui muita vontade",
      "de compartilhar",
      "o pouco que sabe,",
      "e que é fascinado pelo desconhecido!",
      "A lekto foi uma empresa em que contribui com desenvolvimento de software durante um período de 1 ano, nesse período eu aprendi bastante a respeito de regras de negócio e dificuldades técnicas.",
      "Modelagem de banco de dados, deploy de aplicações escaláveis, arquitetura de microsserviços, performance, segurança, tudo isso durante um período excepional e importantissimo para a minha carreira!",
      "BSBIOS é a maior empresa de biodiesel da America Latina, e nela eu participei como consultor e desenvolvedor de software com o objetivo de fornecer manutenção e aprimoramentos no principal sistema de venda e negociações.",
      "Sistemas escritos em AngularJS, PHP e Lumen, Docker e consumo de Websockets com integração de API de terceiros foram os focos dos meus desafios.",
      "Expanzio é um sistema de gerenciamento de grandes franquias aqui no Brasil, trabalhei como consultor e fiz a frente da migração das tecnologias Angular 12 e .NET 3.1 Core.",
      "Partindo pra Angular 15 e .NET 7, tive um período enorme de aprendizado, principalmente com aplicações como Nginx Manager (proxy reverso), Docker, Digital Ocean e integrações.",
      "A Implanta Tecnologia é minha segunda casa, atualmente trabalho aqui e me deparo com os mais variados desafios que já encontrei, desde tecnologias mais atuais, até tecnologias antigas.",
      "Definitivamente um lugar que aprendi bastante sobre como funciona o mundo dos negócios de TI, além dos projetos internos, com tecnologias principais em .NET, Angular e MySQL.",
      "Uma grande universidade de Brasília, no IESB desenvolvi e ofereci suporte para os sistemas internos e externos da empresa, utilizando PHP e Codeigniter, junto com HTML, CSS e Javascript, consegui desenvolver aplicações que otimizam tempo e fornecem produtividade aos colaboradores da empresa.",
      "Desafios como integração e deploy de uma API em Node que faz integração com um sistema de terceiro (Blackboard) foram algumas das minhas missões, aprendi basteante de banco de dados SQL Server, JOBS, modelagem, queries foram um brinde!",
      "Na Clariens participo como consultor e ofereço desenvolvimento em NextJS, Node, e consegui bastante experiência em deploys de sistemas para a Vercel.",
      "Sem dúvidas uma grande colaboradora para o meu desenvolvimento com tecnologias em Node no backend, integração com Hubspot e entendimento sobre sistemas ERP (Lyceum).",
    ],
  },
  STORAGE_KEY: "SELECTED_LANGUAGE",
};

const menus = {
  home: "home",
  career: "career",
};

const definedComponents = [
  {
    key: menus.home,
    onAccess: () => {
      showComponent(menus.home);
    },
  },
  {
    key: menus.career,
    onAccess: () => {
      showComponent(menus.career);
    },
  },
];

// Functions
const showComponent = async (componentName) => {
  const component = transformToComponentName(componentName);
  currContainerId = `${componentName}-container`;
  const containerToShow = document.getElementById(currContainerId);
  const allContainers = document.getElementsByClassName(appContainersClass);

  for (let cont of allContainers) {
    cont.classList.remove("show");
  }

  showContainer(containerToShow);
  highlightMenuItem(`menu-item-${componentName}`);

  if (pushState && !isLocalhost()) {
    history.pushState({}, null, component);
  }
};

const isLocalhost = () =>
  location.host.includes("localhost") ||
  location.host.includes("127.0.0.1") ||
  location.host.includes("192.168");

const showContainer = (containerToShow) =>
  containerToShow.classList.add("show");

const transformToComponentName = (componentName) =>
  definedComponents.find((cn) => cn.key == componentName).key;

const highlightMenuItem = (idMenuItem) => {
  const allMenuItems = document.getElementsByClassName(menuItemChildSelector);

  for (let menu of allMenuItems) {
    menu.previousElementSibling.style.color = textColorCssVar;
    menu.style.color = textColorCssVar;
  }

  const elements = document.getElementsByClassName(idMenuItem);
  for (let element of elements) {
    element.previousElementSibling.style.color = textHoverColorCssVar;
    element.style.color = textHoverColorCssVar;
  }
};

const getPath = () => window.location.pathname.replace("/", "");

const tryShowComponent = () => {
  const component = definedComponents.find((dc) =>
    getPath().toLocaleLowerCase().includes(dc.key)
  );

  if (component != null) {
    component.onAccess();
  } else {
    showComponent(menus.home);
  }
};

const onClickMenuItem = (menuComponent, closeSidebar = false) => {
  if (menuComponent == getPath()) return;

  pushState = true;
  showComponent(menuComponent);

  if (closeSidebar) hideSidebar();
};

const trySelectLanguage = (value, getFromStorage = null) => {
  if (!localStorage.getItem(langs.STORAGE_KEY)) {
    localStorage.setItem(
      langs.STORAGE_KEY,
      location.href.includes("en.souzasaitama") ? langs.EN.key : langs.BR.key
    );
  }

  let language = value;
  if (getFromStorage) language = localStorage.getItem(langs.STORAGE_KEY);
  localStorage.setItem(langs.STORAGE_KEY, language);
  updatePageText(language);
  placeIconOnSelectedLanguage(language);
};

const trySelectTheme = (value, getFromStorage = null) => {
  if (!localStorage.getItem(themes.STORAGE_KEY)) {
    const matchBrowserDefaultTheme =
      window.matchMedia &&
      window.matchMedia("(prefers-color-scheme: dark)").matches;
    localStorage.setItem(
      themes.STORAGE_KEY,
      matchBrowserDefaultTheme ? themes.DARK.key : themes.LIGHT.key
    );
  }

  let theme = value;
  if (getFromStorage) theme = localStorage.getItem(themes.STORAGE_KEY);
  localStorage.setItem(themes.STORAGE_KEY, theme);
  updatePageTheme(theme);
};

const updatePageTheme = (selectedTheme) => {
  const styleEl = document.createElement("style");
  styleEl.id = themes.STORAGE_KEY;
  styleEl.innerHTML = themes[selectedTheme].value;
  const currElement = document.getElementById(themes.STORAGE_KEY);

  if (currElement) {
    currElement.remove();
  }
  document.head.append(styleEl);
};

const placeIconOnSelectedLanguage = (lang) => {
  const selectMarkers = document.getElementsByClassName(
    selectedLanguagePlaceholderCssSelector
  );
  for (let marker of selectMarkers) {
    if (marker.nextElementSibling.id == lang) {
      marker.style.display = "block";
      marker.classList.remove("hidden");
    } else {
      marker.style.display = "none";
    }
  }
};

const updatePageText = (lang) => {
  // Menu
  const elementsToTranslate = document.getElementsByClassName(
    changeLanguageClassSelector
  );
  for (let element of elementsToTranslate) {
    let elementId = element.id;
    element.textContent = findTextLanguageValueById(lang, elementId);
  }

  // Tags
  const childElements = document.querySelectorAll(".replace-text");
  let index = 0;

  for (let i = 0; i < childElements.length; i++) {
    const element = childElements[i];
    if (element.classList.contains("replace-text")) {
      element.textContent = langs[lang].orderText[index];
      index++;
    }
  }
};

const findTextLanguageValueById = (lang, elementId) =>
  langs[lang].values.find((entry) => entry.id == elementId).value;

const toggleTheme = () => {
  const currentTheme = localStorage.getItem(themes.STORAGE_KEY);

  if (currentTheme == themes.DARK.key) {
    trySelectTheme(themes.LIGHT.key, null);
  } else {
    trySelectTheme(themes.DARK.key, null);
  }
};

const selectLanguage = (lang) => trySelectLanguage(lang, null);

const hideSidebar = () => {
  mobileSidebar.classList.add("slide-out");
  setTimeout(() => {
    mobileSidebar.classList.remove("slide-out");
    mobileSidebar.classList.remove("active");
    mobileSidebar.style.transform = `translateX(0%)`;
  }, ANIMATION_MS);
  isSidebarVisible = false;
  changeColorHamburger("var(--misc-color)");
};

const showSidebar = () => {
  mobileSidebar.classList.add("slide-in");
  setTimeout(() => {
    mobileSidebar.classList.remove("slide-in");
    mobileSidebar.classList.add("active");
    mobileSidebar.style.transform = "translateX(0%)";
  }, ANIMATION_MS);
  isSidebarVisible = true;
  changeColorHamburger("var(--misc-color-2)");
};

const toggleMenuMobile = () => {
  canActivateDrag = false;
  if (isSidebarVisible) {
    hideSidebar();
  } else {
    showSidebar();
  }
};

const changeColorHamburger = (color) => {
  for (let bar of barHamburgerIcon) {
    bar.style.backgroundColor = color;
  }
};

const canDragMenu = () => window.outerWidth < 1100;

const handleDrag = (event) => {
  if (!canDragMenu()) return;

  if (isDragging) {
    currentX = event.touches[0].clientX;
    canActivateDrag = mobileSidebar.getBoundingClientRect().x > 0;
    const deltaX = currentX - startX;
    translateX =
      deltaX < 0
        ? Math.min(0, deltaX)
        : Math.max(-mobileSidebar.offsetWidth, deltaX);
    mobileSidebar.style.transform = `translateX(${translateX}px)`;
  }
};

const handleTouchStart = (event) => {
  if (!canDragMenu()) return;
  isDragging = true;
  startX = event.touches[0].clientX;
  currentX = startX;
};

const handleTouchEnd = (e) => {
  if (!canDragMenu() || !canActivateDrag) return;
  isDragging = false;
  const endX = e.changedTouches[0].clientX;
  const deltaX = endX - startX;

  if (deltaX < THRESHOLD_SWIPE_LEFT) {
    showSidebar();
  } else {
    hideSidebar();
  }
  canActivateDrag = false;
};

// Listeners

document.addEventListener("touchstart", handleTouchStart);
document.addEventListener("touchmove", handleDrag);
document.addEventListener("touchend", handleTouchEnd);

window.addEventListener("popstate", () => {
  pushState = false;
  tryShowComponent();
});

document
  .getElementById("hamburger")
  .addEventListener("click", () => toggleMenuMobile());

// Entry

tryShowComponent();
trySelectLanguage(null, true);
trySelectTheme(null, true);
